import axios from 'axios';

export const state = () => ({
  movies: [],
  counter: 1,
  firstInPage: 1,
  lastInPage: 0,
  perInPage: null,
  totalCount: null,
})

export const actions = {
  getMovies({
    commit,
    state
  }) {
    // console.log(state.counter);
    // axios.get("https://api.themoviedb.org/3/movie/now_playing?api_key=37ed43a4f8eaa2abd75f9283692947bc&language=en-US&page=" + `${state.counter}`)
    //   .then((res) => {
    //     console.log(res.data.results);
    //     commit('addMovies', res.data.results)
    //   })
    // axios.get("https://moviesapi.ir/api/v1/movies?page=1")
    //   .then((res) => {
    //     console.log(res.data.data);
    //     commit('addMovies', res.data.data)
    //   })
  },
}
export const mutations = {
  addMovies(state, movies_) {
    // movies_.forEach(movie => {
    //   state.movies.push(movie)
    // });
    state.movies = movies_
    console.log(state.movies);
  },
  setTotalCount(state, total) {
    state.totalCount = total
  },
  setPerInPage(state, perPage) {
    state.perInPage = perPage
  },
  setLastInPage(state) {
    console.log(state.perInPage);
    state.lastInPage = state.lastInPage + state.perInPage
  },
  increaseCounter(state) {
    if (state.lastInPage < 250) {
      state.counter += 1
      state.firstInPage += state.perInPage
      state.lastInPage += state.perInPage
    }
  },
  decreaseCounter(state) {
    if (state.firstInPage > 1) {
      state.counter -= 1
      state.firstInPage -= state.perInPage
      state.lastInPage -= state.perInPage
    }
  }
}
export const getters = {}
